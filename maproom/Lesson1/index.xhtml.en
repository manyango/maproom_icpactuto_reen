<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="en"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>How to make a Maproom page and what can I do with it?</title>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="fr" href="index.html?Set-Language=fr" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<meta property="maproom:Sort_Id" content="a01" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:icon" href="http://digilib.icpac.net/expert/SOURCES/.ENACTS/.version0/.monthly/.climatologies/(.rfe)//var/parameter/interp//name//clim_var/def/SOURCES/.Features/.Political/.ICPAC/.Regions/.the_geom/SOURCES/.Features/.Political/.ICPAC/.Countries/.the_geom/X/Y/fig-/colors/grey/verythin/stroke/black/thin/stroke/-fig//T/last/plotvalue//antialias/true/psdef//plotaxislength/220/psdef//plotborder/0/psdef//color_smoothing/1/psdef//antialias/true/psdef+.gif" />
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Data Library</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/">My 1st Maproom</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>My 1st Maproom</legend> 
                     <span class="navtext">Lesson 1 section</span>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/ICPACcountries.json.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">East Africa</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

 </div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">How to make a Maproom page and what can I do with it?</h2>
<p align="left" property="term:description">This tutorial will teach how to make a Maproom page and what you can put in it.</p>
<p>
As of now, Lesson 1 is an empty Maproom section, that is to say there are no Maprooms sub-sections or pages in it, and 1 empty tab. However, in Lesson1 folder we do have Lesson1.html.lg files. lg denotes a language such as en for English or fr for French and you can choose to work with the language you want.
</p>
<p>
There are three types of files in a Maproom project:
<ul>
<li>.xhtml.lg files;</li>
<li>.html.lg files;</li>
<li>other files.</li>
</ul>
We will see as we go what the other files are for. .html.lg files are the actual Maproom pages that we are interested in making. However, a Maproom website is virtual, if you wish, before it is built (using the make command). The .xhtml.lg files are responsible for the final structure of the Maproom website as the make pulls information from those files and from the .html.lg files to create new temporary (for this very build) html files that will make the Maproom sub-/sections and the navigation links between them. One way to picture it is too consider the .html.lg Maproom pages as the leaves of a tree, and the .xhtml.lg as the trunk and twigs of the tree. Except that the trunk/twigs and leaves structure is not set in stone.
</p>
<p>
This extra step may sound a killer at first but it has several useful purposes:
<ul>
<li>Once you have dozens and dozens of Maproom pages like we do at IRI, this extra step eases the creation of new sub-/sections, pages, or move them around;</li>
<li>Even though the make does set up a final non movable website structure, the whole system allows to make "dynamical" builds. For instance a faceted search interface that will build a Maproom website on the fly, based on user facets choices (e.g. Maprooms about "precipitation" in "Africa"), and on a built-in semantic framework that allows to "tag" Maprooms as such.</li>
</ul>
</p>
<p>
So first of all we need to edit Lesson1.html.lg to have it built properly in our section. Open Lesson1.html.lg with your preferred editor. There are three elements to consider to finalize the building of a Maproom page (or sub-section) into a tab of a section:
<ul>
<li>A semantic tag present in both the "parent" section in which to build and in the "child" page/section to be added. Open the index.xhtml.lg file in your editor and search for tabbedentries typically towards the bottom of the file. You will find the following semantic reference:
<xmp>
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</xmp>
Copy it and go to your Lesson1.html.lg file. Paste it inside the head tag and replace maproom:tabterm by term:isDescribedBy.
</li>
<li>Then the make collects three items from the "child" to build the link to that "child": a title, a description and an icon. The make will look for a piece of text in the "child" that has the tag property:"term:title". This is already set up and you can check that the corresponding text is Lesson 1. The description will be picked up as the piece of text that has the tag property="term:description". This is also already set up. Eventually we need an icon that can be a DL gif or any image. For now we are going to use a non-DL image that we have available in the icons folder. Add the following line in Lesson1.html.lg head tag:
<xmp>
<link rel="term:icon" href="/maproom/icons/Flaticon_5169-32.png" />
</xmp>
</li>
<li>Two fieldsets that will set up in the Maproom Control Bar the navigation back to the "parent" and the navigation to other Maproom pages in the same section. Those navigation items must be present for the integrity of the website. To set them up, copy the followinf lines and paste them in the div with the class controlBar in your Lesson1.html.lg:
<xmp>
<fieldset class="navitem" id="toSectionList">
<legend>Maproom</legend>
<a rev="section" class="navlink carryup" href="/maproom/Lesson1/">Lesson 1 link</a>
</fieldset>
    
<fieldset class="navitem" id="chooseSection">
<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1"><span property="term:label">Lesson 1 label</span></legend>
</fieldset>
</xmp>
The first fieldset legended Maproom is setting up a link named Lesson 1 link back to maproom/Lesson1/. The second fieldset uses the same semantic reference we picked up in the tabbedentries and is setting up a menu to navigate through Maprooms of the same section, and labelling this tab Lesson 1 label. 
</li>
</ul>
We are now ready to build. Save your changes (commit and push if you like) and build the Maproom again.
</p>
</div>

<div class="rightcol tabbedentries" about="/maproom/Lesson1/" >
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
